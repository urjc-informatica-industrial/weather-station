from setuptools import setup

package_name = 'weather_station_ui'
submodules = 'weather_station_ui/ui'

setup(
    name=package_name,
    version='0.0.0',
    packages=[package_name, submodules],
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='diegogd',
    maintainer_email='diego.gdiaz@urjc.es',
    description='UI for weather station',
    license='MIT',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            "weather_station_service = weather_station_ui.service_weather_station:main",
            "weather_station_service_debug = weather_station_ui.service_weather_station:debug"
        ],
    },
)
