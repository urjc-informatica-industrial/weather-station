from threading import Thread
from datetime import datetime
import os
import sys
import signal

try:
    import pandas as pd
    USE_PANDAS=True
except ImportError:
    USE_PANDAS=False   

import rclpy
try:
    import ttkbootstrap as ttk
    from .ui.weather_station_frame import WeatherStationFrame
except:
    print('No ttk available, imposible to run inteface')

from rclpy.node import Node
from weather_station_msgs import srv, msg


class WeatherStationService(Node):

    def __init__(self, station):
        super().__init__('weather_station_service')
        self.station = station
        # self.srv = self.create_service(AddTwoInts, 'add_two_ints', self.add_two_ints_callback)

        self.create_service(srv.UpdateAirTemperature,         
            'weather_station_service/update_air_temperature', self.update_air_temperature)
        self.create_service(srv.UpdateAtmosphericPressure,     
            'weather_station_service/update_atmospheric_pressure', self.update_atmospheric_pressure)
        self.create_service(srv.UpdatePrecipitation,         
            'weather_station_service/update_precipitation', self.update_precipitation)
        self.create_service(srv.UpdateRelativeHumidity, 
            'weather_station_service/update_relative_humidity', self.update_relative_humidity)
        self.create_service(srv.UpdateVisibility, 
            'weather_station_service/update_visibility', self.update_visibility)
        self.create_service(srv.UpdateWindSpeed, 
            'weather_station_service/update_wind_speed', self.update_wind_speed)
        self.create_service(srv.UpdateWindDirection, 
            'weather_station_service/update_wind_direction', self.update_wind_direction)
        self.create_service(srv.UpdateAlertMessage, 
            'weather_station_service/update_alert_message', self.update_alert_message)
        self.create_service(srv.UpdateForecastMessage, 
            'weather_station_service/update_forecast_message', self.update_forecast_message)

    def update_air_temperature(self, 
                            request: srv.UpdateAirTemperature.Request, 
                            response: srv.UpdateAirTemperature.Response):        
        self.station['instance'].air_temperature.setValue(value=request.update.degrees_celsius)
        response.updated = True
        return response   

    def update_atmospheric_pressure(self, 
                            request: srv.UpdateAtmosphericPressure.Request, 
                            response: srv.UpdateAtmosphericPressure.Response):        
        self.station['instance'].atmospheric_pressure.setValue(value=request.update.hectopascals)
        response.updated = True
        return response        
    
    def update_precipitation(self, 
                            request: srv.UpdatePrecipitation.Request, 
                            response: srv.UpdatePrecipitation.Response):        
        self.station['instance'].precipitation.setValue(value=request.update.millimeters)
        response.updated = True
        return response        
    
    def update_relative_humidity(self, 
                            request: srv.UpdateRelativeHumidity.Request, 
                            response: srv.UpdateRelativeHumidity.Response):        
        self.station['instance'].relative_humidity.setValue(value=request.update.percent)
        response.updated = True
        return response        

    def update_visibility(self, 
                        request: srv.UpdateVisibility.Request, 
                        response: srv.UpdateVisibility.Response):        
        self.station['instance'].visibility.setValue(value=request.update.meters)
        response.updated = True
        return response      

    def update_wind_speed(self, 
                        request: srv.UpdateWindSpeed.Request, 
                        response: srv.UpdateWindSpeed.Response):        
        self.station['instance'].wind_speed.setValue(value=request.update.meters_per_second)
        response.updated = True
        return response

    def update_wind_direction(self, 
                        request: srv.UpdateWindDirection.Request, 
                        response: srv.UpdateWindDirection.Response):        
        self.station['instance'].wind_direction.setValue(value=request.update.degrees)
        response.updated = True
        return response

    def update_alert_message(self, request, response):
        self.station['instance'].alert_message.setValue(value=request.update.message)
        response.updated = True
        return response

    def update_forecast_message(self, request, response):
        self.station['instance'].forecast_message.setValue(value=request.update.message)
        response.updated = True
        return response

    def add_two_ints_callback(self, request, response):
        response.sum = request.a + request.b
        self.get_logger().info('Incoming request\na: %d b: %d' % (request.a, request.b))
        self.get_logger().info('Info: %s' % (self.station))
        self.station['instance'].setValue(f"{response.sum}")

        return response

class DumpAttribute:
    def __init__(self, name):
        self.name = name
    def setValue(self, value):
        print(f'{self.name}: Got {value}')

class DumpStationFrame:
    def __getattribute__(self, name: str):
        return DumpAttribute(name)

class PandasAttribute:
    def __init__(self, name, parent):
        self.name = name
        self.parent = parent
    def setValue(self, value):
        metric = {
            'time': datetime.now(),
            'label': self.name,
        }
        if isinstance(value, str):
            metric['message'] = value
        else:
            metric['value'] = value

        self.parent.df = self.parent.df.append(metric, ignore_index=True)

class PandasTrackerStation:
    def __init__(self):
        self.df = pd.DataFrame()

    def __getattr__(self, name: str):
        """
        getattr differs from getattributes as this is executed
        when getattributes doesn't return anything. In this
        case is what we need because we need to access _df.
        """
        return PandasAttribute(name, self)
    def save(self, path):        
        self.df.to_pickle(path)

def exit_gracefully(signum, _kiframe):
    print('captured signal %d' % signum)
    raise(SystemExit)

def debug():
    signal.signal(signal.SIGINT, exit_gracefully)
    signal.signal(signal.SIGTERM, exit_gracefully)
    
    rclpy.init()

    station = {
        'instance': DumpStationFrame()
    }

    if USE_PANDAS:
        station['instance'] = PandasTrackerStation()

    minimal_service = WeatherStationService(station)
    minimal_service.get_logger().info('Service started')
    if USE_PANDAS:
        minimal_service.get_logger().info('Pandas Debug started')
    else:
        minimal_service.get_logger().info('Debug Dummy UI started')

    try:
        rclpy.spin(minimal_service)    
        minimal_service.destroy_node()
        rclpy.shutdown()
    except SystemExit:
        print("Shutdown")
        if USE_PANDAS:
            path=os.getenv('DF_STORE_PATH', '~/debug.pickle')
            station['instance'].save(path)
            print('Save pandas dataframe')
    

def main():
    rclpy.init()

    station = {
        'instance': None
    }

    def createUI():
        app = ttk.Window(
            title="WeatherStation",
            themename="flatly",
            size=(700, 500),
            resizable=(False, False),
        )
        station['instance'] = WeatherStationFrame(app)

        def on_closing():
            app.destroy()
            rclpy.shutdown()
            exit()

        app.protocol("WM_DELETE_WINDOW", on_closing)

        app.mainloop()
        

    t = Thread(target=createUI, args=[])
    t.start()

    print(station)
    minimal_service = WeatherStationService(station)
    minimal_service.get_logger().info('Service started')
    minimal_service.get_logger().info('UI started')

    rclpy.spin(minimal_service)
    minimal_service.destroy_node()

    rclpy.shutdown()


if __name__ == '__main__':
    main()