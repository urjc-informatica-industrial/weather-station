import ttkbootstrap as ttk
from ttkbootstrap.constants import *
from ttkbootstrap.tooltip import ToolTip
from ttkbootstrap.toast import ToastNotification

class WeatherStationFrame(ttk.Frame):
    def __init__(self, master, **kwargs):
        super().__init__(master, padding=(20,10), **kwargs)
        self.pack(fill=BOTH, expand=YES)
        # ttk.Style().configure("TButton", font="TkFixedFont 12")

        alert_area = ttk.Labelframe(self, bootstyle="danger", text="Alertas")
        command = 'ros2 service call /weather_station_service/update_alert_message weather_station_msgs/srv/UpdateAlertMessage "{update: { message: \'Hello\' }}"'
        self.alert_message = TextWidget(alert_area, clipboard=command)
        self.alert_message.pack(fill=X, expand=YES, padx=5, pady=5)
        ToolTip(self.alert_message, text=command)
        alert_area.pack(fill=X, expand=YES)

        # ROW 2
        row = ttk.Frame(self)        
        command = 'ros2 service call /weather_station_service/update_air_temperature weather_station_msgs/srv/UpdateAirTemperature "{update: { degrees_celsius: 13.0 }}"'
        self.air_temperature = TemperatureIndicator(row, "Air temp.", clipboard=command)
        ToolTip(self.air_temperature, text=command)
        # self.air_temperature.bind('<Button-1>', self.air_temperature.set_message_to_clipboard)
        self.air_temperature.pack(padx=40, side=LEFT)

        command = 'ros2 service call /weather_station_service/update_atmospheric_pressure weather_station_msgs/srv/UpdateAtmosphericPressure "{update: { hectopascals: 715 }}"'
        self.atmospheric_pressure = AtmosphericPressureIndicator(row, "Atm. Pressure", clipboard=command)
        ToolTip(self.atmospheric_pressure, text=command)
        self.atmospheric_pressure.pack(padx=40, side=LEFT)
        command = 'ros2 service call /weather_station_service/update_precipitation weather_station_msgs/srv/UpdatePrecipitation "{update: { millimeters: 20.0 }}"'
        self.precipitation = PrecipitationIndicator(row, "Precipitations", clipboard=command)
        ToolTip(self.precipitation, text=command)
        self.precipitation.pack(padx=40, side=LEFT)
        row.pack(fill=X, expand=YES, pady=5)

        # Row 3
        row = ttk.Frame(self)
        command = 'ros2 service call /weather_station_service/update_relative_humidity weather_station_msgs/srv/UpdateRelativeHumidity "{update: { percent: 25 }}"'
        self.relative_humidity = RelativeHumidityIndicator(row, "Humidity", clipboard=command)
        ToolTip(self.relative_humidity, text=command)
        self.relative_humidity.pack(padx=10, side=LEFT)
        command = 'ros2 service call /weather_station_service/update_visibility weather_station_msgs/srv/UpdateVisibility "{update: { meters: 60.5 }}"'
        self.visibility = VisibilityIndicator(row, "Visibility", clipboard=command)
        ToolTip(self.visibility, text=command)
        self.visibility.pack(padx=10, side=LEFT)
        # Block for wind
        block = ttk.Frame(row)        
        command = 'ros2 service call /weather_station_service/update_wind_speed weather_station_msgs/srv/UpdateWindSpeed "{update: { meters_per_second: 15.0 }}"'
        self.wind_speed = WindSpeedIndicator(block, label="Wind speed", clipboard=command)
        ToolTip(self.wind_speed, text=command)
        self.wind_speed.pack(padx=5, side=LEFT)
        command = 'ros2 service call /weather_station_service/update_wind_direction weather_station_msgs/srv/UpdateWindDirection "{update: { degrees: 90 }}"'
        self.wind_direction = WindDirectionIndicator(block, label="Wind direction", clipboard=command)
        ToolTip(self.wind_direction, text=command)
        self.wind_direction.pack(padx=5, side=LEFT)
        self.wind_direction.setValue(90)
        # End block
        block.pack(padx=20, side=LEFT)        
        row.pack(fill=X, expand=YES, pady=5)

        forecast_area = ttk.Labelframe(self, bootstyle="secondary", text="Pronóstico meteorológico")
        command = 'ros2 service call /weather_station_service/update_forecast_message weather_station_msgs/srv/UpdateForecastMessage "{update: { message: \'It will rain\' }}"'
        self.forecast_message = TextWidget(forecast_area, clipboard=command)
        self.forecast_message.pack(fill=X, expand=YES, padx=5, pady=5)
        ToolTip(self.forecast_message, text=command)
        forecast_area.pack(fill=X, expand=YES)


class CopiableWidget(ttk.Frame):
    def __init__(self, master, clipboard):
        super().__init__(master, takefocus=True)        
        self.clipboard_message = clipboard        

    def set_message_to_clipboard(self, event):
        print(self.clipboard_message)

        self.clipboard_clear()
        self.clipboard_append(self.clipboard_message)
        
        toast = ToastNotification(
            title="WeatherStation - Copy Service",
            message=f"Message copied: {self.clipboard_message}",
            duration=3000,
        )
        toast.show_toast()

    def set_copy_on_click(self, widget):
        widget.bind("<Button-1>", self.set_message_to_clipboard)
    

class TemperatureIndicator(CopiableWidget):
    def __init__(self, master, label, clipboard=None):
        super().__init__(master, clipboard)

        self.meter = ttk.Meter(
            master=self,
            metersize=140,
            amountused=36.7,
            amounttotal=60,
            metertype="semi",
            textright="ºC",
            subtext=label,
            meterthickness=5,
            bootstyle='success'
        )
        self.meter.pack()
        self.set_copy_on_click(self.meter.indicator) 
        self.set_copy_on_click(self.meter.textframe)               

    def setValue(self, value):
        self.meter.configure(amountused=value)

class AtmosphericPressureIndicator(CopiableWidget):
    def __init__(self, master, label, clipboard=None):
        super().__init__(master, clipboard)

        self.meter = ttk.Meter(
            master=self,
            metersize=140,
            amountused=700,
            bootstyle="secondary",
            textright="hPa",
            metertype="semi",
            amounttotal=1000,
            subtext=label,
            meterthickness=5
        )
        self.meter.pack()
        self.set_copy_on_click(self.meter.indicator)
        self.set_copy_on_click(self.meter.textframe)

    def setValue(self, value):
        self.meter.configure(amountused=value)

class RelativeHumidityIndicator(CopiableWidget):
    def __init__(self, master, label, clipboard=None):
        super().__init__(master, clipboard)

        self.meter = ttk.Meter(
            master=self,
            metersize=140,
            amountused=10,
            metertype="full",
            textright="%",
            subtext=label,
            amounttotal=100,
            meterthickness=5,
            stepsize=1,
            stripethickness=5,
            arcoffset=90
        )
        self.meter.pack()
        self.set_copy_on_click(self.meter.indicator)
        self.set_copy_on_click(self.meter.textframe)

    def setValue(self, value):
        self.meter.configure(amountused=value)

class VisibilityIndicator(CopiableWidget):
    def __init__(self, master, label, clipboard=None):
        super().__init__(master, clipboard)

        self.meter = ttk.Meter(
            master=self,
            metersize=140,
            amountused=20,
            metertype="full",
            textright="m",
            subtext=label,
            amounttotal=100,
            meterthickness=5,
            arcrange=300,
            arcoffset=210,
            stepsize=1
        )
        self.meter.pack()
        self.set_copy_on_click(self.meter.indicator)
        self.set_copy_on_click(self.meter.textframe)

    def setValue(self, value):
        self.meter.configure(amountused=value)

class PrecipitationIndicator(CopiableWidget):
    def __init__(self, master, label, clipboard=None):
        super().__init__(master, clipboard)

        self.meter = ttk.Meter(
            master=self,
            metersize=140,
            amountused=0,
            metertype="semi",
            textright="mm",
            subtext=label,
            amounttotal=1000,
            meterthickness=5,
            bootstyle='info',
            stepsize=1
        )
        self.meter.pack()
        self.set_copy_on_click(self.meter.indicator)
        self.set_copy_on_click(self.meter.textframe)

    def setValue(self, value):
        self.meter.configure(amountused=value)

class WindSpeedIndicator(CopiableWidget):
    def __init__(self, master, label, clipboard=None):
        super().__init__(master, clipboard)

        self.meter = ttk.Meter(
            master=self,
            metersize=140,
            amountused=20,
            metertype="semi",
            textright="m/s",
            subtext=label,
            amounttotal=90,
            meterthickness=5,
            bootstyle='warning',
            stepsize=1
        )        
        self.meter.pack()
        self.set_copy_on_click(self.meter.indicator)
        self.set_copy_on_click(self.meter.textframe)

    def setValue(self, value):
        self.meter.configure(amountused=value)

class WindDirectionIndicator(CopiableWidget):
    def __init__(self, master, label, clipboard=None):
        super().__init__(master, clipboard)

        self.meter = ttk.Meter(
            master=self,
            metersize=140,
            amountused=20,
            metertype="full",
            textright="ºN",
            subtext=label,
            amounttotal=359,
            meterthickness=5,
            bootstyle='warning',
            stepsize=1
        )        
        self.meter.pack()
        self.set_copy_on_click(self.meter.indicator)
        self.set_copy_on_click(self.meter.textframe)

    def setValue(self, value):
        self.meter.configure(amountused=value)

class TextWidget(CopiableWidget):
    def __init__(self, master, clipboard=None):
        super().__init__(master, clipboard)

        self.text_message = ttk.Label(self, text="Ninguna alerta.")    
        self.text_message.pack(fill=X, expand=YES, padx=5, pady=5)    
        self.set_copy_on_click(self.text_message)

    def setValue(self, value):
        self.text_message.configure(text=value)
