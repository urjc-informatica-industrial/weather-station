# Weather station

Learning module for ROS development for URJC Course.

## How to install

Download the project following these steps:

To prepare your environment access to the [lecture documentation](https://infind-urjc-slides.netlify.app/quick-guides/ros.html)

```sh
cd ~/ros2_ws/src
git clone git@gitlab.com:urjc-informatica-industrial/weather-station.git
cd ..
colcon build --packages-select weather_station_msgs weather_station_ui
```


# Weather metrics

From the (windy.app website](https://windy.app/blog/weather-units-of-measurement.html):

- Air temperature: degrees Celsius (°C) or Kelvins (K). Other units: Fahrenheit (ºF).
- Precipitation (total amount): millimeters (mm) or kilograms per square meter (kg/sq m). Other units: inches (in).
- Wind speed: meters per second (m/s). Other units: miles per hour (mph), kilometers per hour (kph), knots (knt = 0.514 m/s, 1.15078 mph, 1.852 kph, 1 nautical mile per hour), beaufort (Beaufort wind force scale).
- Wind direction: degrees clockwise from north or on the scale 0-36(0), where 36(0) is the wind from the north and 9(0) is the wind from the east (°). Other units: cardinal directions (north (N), south (S), west (W), east (E) and others).
- Atmospheric pressure: hectopascals (hPa). Other units: millimeter of mercury (mmHg), inches of mercury (inHg).
- Relative humidity: per cent (%).
- Visibility: meters (m). Other units: kilometers (km), feet (ft).
- Sunshine duration: hours (h). Other units: minutes (min), seconds (s).

Other:
- Cloud height: metres (m). Other units: feet (ft).
- Cloud cover (amount): oktas. Other units: per cent (%).
- Precipitation intensity (Ri): millimetres per hour (mm/h) or kilograms per m-2 per second (kg/sq m/s).
- Snow water equivalent: kilograms per square meter (kg/sq m).
- Evaporation: millimetres (mm).
- Irradiance: watts per square meter and radiant exposure in joules per square meter (W/sq m, J/sq m).
- Geopotential, used in upper-air observations: standard geopotential metres (m′). Note: height, level or altitude are presented with respect to a well-defined reference. Typical references are Mean Sea Level (MSL), station altitude or the 1013.2 hPa plane.

