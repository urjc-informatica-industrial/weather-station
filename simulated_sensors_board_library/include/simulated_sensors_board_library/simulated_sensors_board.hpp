#ifndef SIMULATED_SENSORS_BOARD__SIMULATED_SENSORS_BOARD_HPP_
#define SIMULATED_SENSORS_BOARD__SIMULATED_SENSORS_BOARD_HPP_

#include <memory>


class Oscillator 
{
public:
  Oscillator(int, int, int);
  int get_initial_value() const;
  int read_pin_value() const;
  void print() const;
private:
  int min_;
  int max_;
  int variation_;
  int initial_value_;
  int x_gap_ {0};
  int oscilation_function_ {1}; 
};

class SimulatedSensorsBoard
{
public:
  SimulatedSensorsBoard();
  int read_pin_value(int pin);
  void print() const;

  // Constants
  static const int PIN_TEMPERATURE;
  static const int PIN_PRESSURE;
  static const int PIN_HUMIDITY;
  static const int PIN_PRECIPITATION;
  static const int PIN_WIND_SPEED;
  static const int PIN_WIND_DIRECTION;
  static const int PIN_VISIBILITY;

private:
  int seed;
  std::shared_ptr<Oscillator> air_temperature_;
  std::shared_ptr<Oscillator> atmospheric_pressure_;
  std::shared_ptr<Oscillator> relative_humidity_;
  std::shared_ptr<Oscillator> precipitation_;
  std::shared_ptr<Oscillator> wind_speed_;
  std::shared_ptr<Oscillator> wind_direction_;
  std::shared_ptr<Oscillator> visibility_;
};

#endif // SIMULATED_SENSORS_BOARD__SIMULATED_SENSORS_BOARD_HPP_