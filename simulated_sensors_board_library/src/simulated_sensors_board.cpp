#include "simulated_sensors_board.hpp"

#include <boost/chrono/chrono_io.hpp>
#include <stdio.h>      /* printf, NULL */
#include <stdlib.h>     /* srand, rand */
#include <time.h>       /* time */
#include <stdlib.h>     /* getenv */
#include <chrono>
#include <ctime>
#include <cmath>

#include <iostream>

// Constants
const int SimulatedSensorsBoard::PIN_TEMPERATURE = 1;
const int SimulatedSensorsBoard::PIN_PRESSURE = 2;
const int SimulatedSensorsBoard::PIN_HUMIDITY = 3;
const int SimulatedSensorsBoard::PIN_PRECIPITATION = 4;
const int SimulatedSensorsBoard::PIN_WIND_SPEED = 5;
const int SimulatedSensorsBoard::PIN_WIND_DIRECTION = 6;
const int SimulatedSensorsBoard::PIN_VISIBILITY = 7;

int rand_from_limits(int min, int max, int variation) {
  auto low_limit = static_cast<int>(min + variation/2);
  auto top_limit = static_cast<int>(max - variation/2);
  return low_limit + rand() % (top_limit - low_limit + 1);
}

auto start_time = std::chrono::system_clock::now();

long int getCurrentTime() {
  // std::cout << "Hello world"<< std::endl;
  // auto start_time_ms = std::chrono::duration_cast<std::chrono::milliseconds>(start_time.time_since_epoch()).count();
  // printf(":: %ld :: ", start_time_ms);
  auto current_time = std::chrono::system_clock::now();
  auto elapsed_time_ms = std::chrono::duration_cast<std::chrono::milliseconds>(current_time - start_time).count();
  
  return elapsed_time_ms;
}

Oscillator::Oscillator(int min, int max, int variation) : min_{min}, max_{max}, variation_{variation} {
    initial_value_ = rand_from_limits(min_, max_, variation_);
    oscilation_function_ = std::rand() % 6 + 1;
    x_gap_ = std::rand() % 100000;
}

int Oscillator::read_pin_value() const {
    auto now = getCurrentTime();
    double calculation = 0.0;

    double x = (now / 10000.0) + x_gap_;
    switch (oscilation_function_)
    {
    case 1:
      calculation = initial_value_ + (variation_/2 * sin(M_PI * x));
      break;
    case 2:
      calculation = initial_value_ + (variation_/2 * ( (cos(x/3) + cos(x) + sin(3*x))/3 ));
      break;
    case 3:
      calculation = initial_value_ + (variation_/2 * (  (cos(x/3) + cos(x + (x/2)) + sin(3*x-(x/4)))/3));
      break;
    case 4:
      calculation = initial_value_ + (variation_/2 * ( (cos(x + (x/3)) + sin(2*x-(x/4)))/2 ));
      break;
    case 5:
      calculation = initial_value_ + (variation_/2 * ( (sin(x + (x/3)) + sin(x + (x/2)) + sin( 4*x - (x/4)))/3 ));
      break;
    case 6:
      calculation = initial_value_ + (variation_/2 * ( (sin(x + (x/3)) + sin(x - (x/7)) + sin( 4*x - (x/4)))/3 ));
      break;
    
    default:
      break;
    }
    
    
    if (calculation > max_) { calculation = max_; }
    if (calculation < min_) { calculation = min_; }

    auto return_value = ((calculation - min_) / (max_ - min_))  * 1023;

    return return_value;
}

int Oscillator::get_initial_value() const {
  return initial_value_;
}

void Oscillator::print() const {
  printf("val: %d, min: %d, max: %d, var: %d", initial_value_, min_, max_, variation_);
}

SimulatedSensorsBoard::SimulatedSensorsBoard() {
  int seed;
  char* env_seed = std::getenv ("SIM_LIB_SEED");
  if (env_seed != NULL) {
    seed = atoi(env_seed);
  } else {
    seed = time(NULL);    
  }
  srand(seed);

  air_temperature_ = std::make_shared<Oscillator>(-20, 60, 30);
  atmospheric_pressure_ = std::make_shared<Oscillator>(700, 1100, 300);
  relative_humidity_ = std::make_shared<Oscillator>(0, 100, 50);
  precipitation_ = std::make_shared<Oscillator>(0, 100, 40);
  wind_speed_ = std::make_shared<Oscillator>(0, 90, 40);
  wind_direction_ = std::make_shared<Oscillator>(0, 359, 180);
  visibility_ = std::make_shared<Oscillator>(0, 100, 40);  
}

void SimulatedSensorsBoard::print() const {
  printf("\n----------------\n");
  printf("air_temperature: "); air_temperature_->print();
  printf("\natmospheric_pressure: "); atmospheric_pressure_->print();
  printf("\nrelative_humidity: "); relative_humidity_->print();
  printf("\nprecipitation: "); precipitation_->print();
  printf("\nwind_speed: "); wind_speed_->print();
  printf("\nwind_direction: "); wind_direction_->print();
  printf("\nvisibility: "); visibility_->print();
  printf("\n----------------\n");
}

int SimulatedSensorsBoard::read_pin_value(int pin) {
  switch (pin)
  {
    case SimulatedSensorsBoard::PIN_TEMPERATURE:
      return air_temperature_->read_pin_value();
      break;
    case SimulatedSensorsBoard::PIN_PRESSURE:
      return atmospheric_pressure_->read_pin_value();
      break;
    case SimulatedSensorsBoard::PIN_HUMIDITY:
      return relative_humidity_->read_pin_value();
      break;
    case SimulatedSensorsBoard::PIN_PRECIPITATION:
      return precipitation_->read_pin_value();
      break;
    case SimulatedSensorsBoard::PIN_WIND_SPEED:
      return wind_speed_->read_pin_value();
      break;
    case SimulatedSensorsBoard::PIN_WIND_DIRECTION:
      return wind_direction_->read_pin_value();
      break;
    case SimulatedSensorsBoard::PIN_VISIBILITY:
      return visibility_->read_pin_value();
      break;  
    default:
      break;
  }

  return -1;
}
