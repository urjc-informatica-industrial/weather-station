#include <iostream>
#include <thread>

#include "simulated_sensors_board.hpp"

int main(int argc, char ** argv)
{
  (void) argc;
  (void) argv;

  auto board = SimulatedSensorsBoard();
  board.print();

  for (int i=0; i < 10; i++) {
    auto temperature = board.read_pin_value(board.PIN_TEMPERATURE);
    std::cout << "Temperature: " << temperature << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }

  for (int i=0; i < 10; i++) {
    auto value = board.read_pin_value(board.PIN_PRESSURE);
    std::cout << "Pressure: " << value << std::endl;
    std::this_thread::sleep_for(std::chrono::milliseconds(10));
  }
  
  return 0;
}

// colcon build --packages-select simulated_sensors_board_library && ros2 run simulated_sensors_board_library simulated_sensors_board_test
// ros2 run simulated_sensors_board_library simulated_sensors_board_test
// SIM_LIB_SEED=12 ros2 run simulated_sensors_board_library simulated_sensors_board_test
